## Docs file relayer
A simple mechanism to build links to the documentation directory of other repository in the same folder

### Installation
Clone from the repository
```
$ git clone https://kuanpern@bitbucket.org/kuanpern/docsfile_relayer.git
```

Install required packages
```
$ cd docsfile_relayer
$ virtualenv -ppython3 venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

### Usage
Use the following to create / refresh documentation file links.
```
$ ./generate_all.py
```

This package will create a `docs` directory and build soft links to the `/docs/_build/html` of all repositories in the same directory of this package. An `index.html` file contains appropriate hyperlinks will also be generated.

The documentation directory can be served with nginx. A sample configuration `sample.nginx` is provided with the repo. 

Make the appropriate modifications (e.g. change `server_name` and SSL settings) and put the files to `/etc/nginx/sites-available/` (softlink to `/etc/nginx/sites-enabled/` and reload ngnix setting for this to take effect).




