#!/usr/bin/env python3
import os
import glob
import jinja2

# build links
os.system('./build_links.sh')

# get valid links
valid_links  = []
broken_links = []

files = glob.glob('./docs/*')
for filename in files:
	basename = os.path.basename(filename)
	if os.path.islink(filename):
		if not os.path.exists(filename):
			broken_links.append(basename)
		else:
			valid_links.append(basename)
		# end if
	# end if
# end for

# read template
with open('templates/index.html', 'r') as fin:
	t = jinja2.Template(fin.read())
# end for

# render
content = t.render(broken_links=broken_links, valid_links=valid_links)
with open('index.html', 'w') as fout:
	fout.write(content)
# end with
